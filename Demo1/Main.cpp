// Tim Yang
// *Demo 1*

#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	// "<<" is called a stream indicator

	cout << "Hello world!\n";

	int i;
	float f;
	double d;
	bool b;
	char c;
	
	// const, both ways below are correct.
	// const has multiple uses/fucntions in C++
	const double PI = 3.141926;
	// double const PI = 3.141926;

	cout << "Enter an integer.";
	cin >> i;

	if (i > 100) cout << "That's a big number.\n";
	else cout << "That's not a big number.\n";

	if (i) cout << "I is not zero.\n";

	// switch case example online

	for (int i = 0; i <= 10; i++)
	{
		cout << i << "\n";
	}

	cout << "Do you want to do math? (y/n):";
	int x = 1;
	char input;
	cin >> input;

	while (input == 'y')
	{
		cout << "9 x " << x << " = " << 9 * x << "\n";
		cout << "Again?";
		cin >> input;
		// ++x is preincrementing and works the same way as x++
			// IF it is on a line on its own. 
		x++;
	}



	_getch();
	return 0;
}